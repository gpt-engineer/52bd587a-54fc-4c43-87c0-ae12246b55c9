document.getElementById('task-form').addEventListener('submit', function(event) {
  event.preventDefault();

  const taskInput = document.getElementById('task-input');
  const taskList = document.getElementById('task-list');

  const newTask = document.createElement('li');
  newTask.textContent = taskInput.value;
  newTask.classList.add('bg-white', 'px-3', 'py-2', 'border', 'rounded', 'flex', 'justify-between', 'items-center');

  const deleteButton = document.createElement('button');
  deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
  deleteButton.classList.add('text-red-500');
  deleteButton.addEventListener('click', function() {
    taskList.removeChild(newTask);
  });

  newTask.appendChild(deleteButton);
  taskList.appendChild(newTask);

  taskInput.value = '';
});
